config {
    sourceopt {
        removeComments {
            keep {
                20 = /^CACHED_/usi
            }
        }
    }
}